# Argum

A package for getting command line arguments in a simple way.

## Install

Install with

    npm install --save argum

## How to use

First require argum

    var argum = require('argum');
    
then to get an argument's value use this

    var argumentValue = argum.get(<prefix>, <required>, <defaultValue>)
    
where 'prefix' is the prefix with which the argument starts (note that the prefix 
and actual value must be separated by equals sign '='). 'required' is whether the argument
is required or not, if it is required and the argument is not passed in argum will
throw an error. 'defaultValue' is the default value in case of the argument
not being required.

## Example

Given a command line call like this one

    node index.js --name=Petar --surname=Vasilev
    
and the following code

    var argum = require('argum');  
    
    var config = {
        name: argum.get('--name', true, false),
        surname: argum.get('--surname', true, false),
        race: argum.get('--race', false, 'human')
    }
    
config.name will have a value of 'Petar'. config.surname will have a value of 'Vasilev'
and config.race will have a value of 'human'.

There is now an 'object' method as well which can be used to get values from an object literal
in much the same way the 'get' method is used to get command line arguments. For example this:

    var argum = require('argum');
    var options = {name: 'Petar', surname: 'Vasilev'};
    
    var config = {
        name: argum.object(options, 'name', true, false),
        surname: argum.object(options, 'surname', true, false),
        race: argum.object(options, 'race', false, 'human')
    }
    
will again respectively set 'name' to 'Petar', 'surname' to 'Vasilev' and 'race' to 'human'.

## License

MIT License

Copyright 2020 [Petar Vasilev](https://www.petarvasilev.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.