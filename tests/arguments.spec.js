'use strict';

const argum = require('../argum');
const expect = require('chai').expect;

describe('Main module', function() {
    describe('testing --name=Petar', function() {
        it('returns a value', function() {
            process.argv = [
                '--name=Petar'
            ];

            expect(argum.get('--name', true, false)).to.be.equal('Petar');
        });
    });

    describe('testing --surname=Vasilev', function() {
        it('returns a value', function() {
            process.argv = [
                '--surname=Vasilev'
            ];

            expect(argum.get('--surname', true, false)).to.be.equal('Vasilev');
        });
    });

    describe('testing empty space in value', function() {
        it('returns a value', function() {
            process.argv = [
                '-s=This is a test'
            ];

            expect(argum.get('-s', true, false)).to.be.equal('This is a test');
        });
    });

    describe('testing equals sign in argument value', function() {
        it('returns a value', function() {
            process.argv = [
                '--equals=test1=test2'
            ];

            expect(argum.get('--equals', true, false)).to.be.equal('test1=test2');
        });
    });

    describe('testing no arguments passsed', function() {
        it('returns a value', function() {
            process.argv = [];

            expect(argum.get('--race', false, 'Human')).to.be.equal('Human');
        });
    });
});

describe('Object module', function() {
    describe('testing object value not required', function() {
        it('returns a value', function() {
            expect(argum.object({'--name': 'Petar'}, '--name', false, false)).to.be.equal('Petar');
        });
    });

    describe('testing object value required', function() {
        it('returns a value', function() {
            expect(argum.object({'--name': 'Petar'}, '--name', true, false)).to.be.equal('Petar');
        });
    });

    describe('testing object default value', function() {
        it('returns a value', function() {
            expect(argum.object({}, '--name', false, 'Valentin')).to.be.equal('Valentin');
        });
    });

    describe('testing error on option missing', function() {
        it('returns a value', function() {
            expect(function() {
                argum.object({'--surname': 'Vasilev'}, '--name', true, false)
            }).to.throw('Required parameter missing: --name');
        });
    });

    describe('testing non-object error', function() {
        it('returns a value', function() {
            expect(function() {
                argum.object('test', '--name', false, 'default')
            }).to.throw('First argument is not an object.');
        });
    });

    describe('testing not required but no default value either returning false', function() {
        it('returns a value', function() {
            expect(argum.object({}, '--name', false, false)).to.be.equal(false);
        });
    });
});